/*****
* CONFIGURATION
*/
  'use strict';
  
  $.panelIconOpened = 'icon-arrow-up';
  $.panelIconClosed = 'icon-arrow-down';

  //Default colours
  $.brandPrimary =  '#20a8d8';
  $.brandSuccess =  '#4dbd74';
  $.brandInfo =     '#63c2de';
  $.brandWarning =  '#f8cb00';
  $.brandDanger =   '#f86c6b';

  $.grayDark =      '#2a2c36';
  $.gray =          '#55595c';
  $.grayLight =     '#818a91';
  $.grayLighter =   '#d1d4d7';
  $.grayLightest =  '#f8f9fa';

//生成菜单
var menuItem = Vue.extend({
	name: 'menu-item',
	props:{item:{}},
	template:[
	          '<li class="nav-item nav-dropdown" v-if="item.type === 0">',
	          '<a class="nav-link nav-dropdown-toggle" href="javascript:;">',
	          '<i v-if="item.icon != null" :class="item.icon"></i>',
	          '<span>{{item.name}}</span>',
	          //'<i class="fa fa-angle-left pull-right"></i>',
	          '</a>',
	          '<ul class="nav-dropdown-items">',
	          '<menu-item :item="item" v-for="item in item.list"></menu-item>',
	          '</ul>',
	          '</li>',
	          '<li class="nav-item" v-else-if="item.type === 1">',
	          '<a :href="\'/\'+item.url" class="nav-link"><i v-if="item.icon != null" :class="item.icon"></i><i v-else class="fa fa-circle-o"></i> {{item.name}}</a>',
	          '</li>',
	].join('')
});
Vue.component('menuItem',menuItem);

/****
* MAIN NAVIGATION
*/

$(document).ready(function($){
	$.ajaxSetup({
		dataType: "json",
		contentType: "application/json",
		cache: false
	});

	$.navigation = $('#rrapp nav > ul.nav');
	  
	 var $navigation_a = $.navigation.find('a');
	  //标记当前菜单
	  $navigation_a.each(function(){

		    var cUrl = String(location).split('?')[0];

		    if (cUrl.substr(cUrl.length - 1) == '#') {
		      cUrl = cUrl.slice(0,-1);
		    }

		    if ($($(this))[0].href==cUrl) {
		      $(this).addClass('active');

		      $(this).parents('ul').add(this).each(function(){
		        $(this).parent().addClass('open');
		      });
		    }
		 
	   });
	  
	  // Dropdown Menu
	  $.navigation.on('click', 'a', function(e){
		
	    if ($.ajaxLoad) {
	      e.preventDefault();
	    }

	    if ($(this).hasClass('nav-dropdown-toggle')) {
	      $(this).parent().toggleClass('open');
	      resizeBroadcast();
	    }

	  });

	  function resizeBroadcast() {

	    var timesRun = 0;
	    var interval = setInterval(function(){
	      timesRun += 1;
	      if(timesRun === 5){
	        clearInterval(interval);
	      }
	      window.dispatchEvent(new Event('resize'));
	    }, 62.5);
	  }

  /* ---------- Main Menu Open/Close, Min/Full ---------- */
  $('.navbar-toggler').on('click',function(){

    if ($(this).hasClass('sidebar-toggler')) {
      $('body').toggleClass('sidebar-hidden');
      resizeBroadcast();
    }

    if ($(this).hasClass('sidebar-minimizer')) {
      $('body').toggleClass('sidebar-compact');
      resizeBroadcast();
    }

//    if ($(this).hasClass('aside-menu-toggler')) {
//      $('body').toggleClass('aside-menu-hidden');
//      resizeBroadcast();
//    }

    if ($(this).hasClass('mobile-sidebar-toggler')) {
      $('body').toggleClass('sidebar-mobile-show');
      resizeBroadcast();
    }

  });

  $('.sidebar-close').click(function(){
    $('body').toggleClass('sidebar-opened').parent().toggleClass('sidebar-opened');
  });

  /* ---------- Disable moving to top ---------- */
  $('a[href="#"][data-top!=true]').click(function(e){
    e.preventDefault();
  });

});

/****
* CARDS ACTIONS
*/

//$(document).on('click', '.card-actions a', function(e){
//  e.preventDefault();
//
//  if ($(this).hasClass('btn-close')) {
//    $(this).parent().parent().parent().fadeOut();
//  } else if ($(this).hasClass('btn-minimize')) {
//    var $target = $(this).parent().parent().next('.card-block');
//    if (!$(this).hasClass('collapsed')) {
//      $('i',$(this)).removeClass($.panelIconOpened).addClass($.panelIconClosed);
//    } else {
//      $('i',$(this)).removeClass($.panelIconClosed).addClass($.panelIconOpened);
//    }
//
//  } else if ($(this).hasClass('btn-setting')) {
//    $('#myModal').modal('show');
//  }
//
//});

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function init(url) {

  /* ---------- Tooltip ---------- */
  $('[rel="tooltip"],[data-rel="tooltip"]').tooltip({"placement":"bottom",delay: { show: 400, hide: 200 }});

  /* ---------- Popover ---------- */
  $('[rel="popover"],[data-rel="popover"],[data-toggle="popover"]').popover();

}
