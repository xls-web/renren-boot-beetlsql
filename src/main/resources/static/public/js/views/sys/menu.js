$(function () {
	$.jgrid.defaults.width = 1000;
	$.jgrid.defaults.responsive = true;
	$.jgrid.defaults.styleUI = 'Bootstrap';
	
	var $jqGrid = $("table#jqGrid");
	
	$jqGrid.jqGrid({
        url: '../sys/menu/list',
        datatype: "json",
        colModel: [			
			{ label: '菜单ID', name: 'menuId', index: "menu_id", width: 40, key: true },
			{ label: '菜单名称', name: 'name', width: 60 },
			{ label: '上级菜单', name: 'parentName', sortable: false, width: 60 },
			{ label: '菜单图标', name: 'icon', sortable: false, width: 50, formatter: function(value, options, row){
				return value == null ? '' : '<i class="'+value+' fa-lg"></i>';
			}},
			{ label: '菜单URL', name: 'url', width: 100 },
			{ label: '授权标识', name: 'perms', width: 100 },
			{ label: '类型', name: 'type', width: 50, formatter: function(value, options, row){
				if(value === 0){
					return '<span class="label label-primary">目录</span>';
				}
				if(value === 1){
					return '<span class="label label-success">菜单</span>';
				}
				if(value === 2){
					return '<span class="label label-warning">按钮</span>';
				}
			}},
			{ label: '排序号', name: 'orderNum', index: "order_num", width: 50}
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$jqGrid.closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var setting = {
	data: {
		simpleData: {
			enable: true,
			idKey: "menuId",
			pIdKey: "parentId",
			rootPId: -1
		},
		key: {
			url:"nourl"
		}
	}
};
var ztree;

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
		menu:{
			parentName:null,
			parentId:0,
			type:1,
			orderNum:0
		},
		menuList:store.get('menuList'),
		permissions:store.get('permissions'),
		username:store.get('user').username,
		password:'',
		newPassword:''
	},
	methods: {
		getMenu: function(menuId){
			//加载菜单树
			$.get("../sys/menu/select", function(r){
				ztree = $.fn.zTree.init($("#menuTree"), setting, r.menuList);
				var node = ztree.getNodeByParam("menuId", vm.menu.parentId);
				ztree.selectNode(node);
				
				vm.menu.parentName = node.name;
			})
		},
		add: function(){
			this.showList = false;
			this.title = "新增";
			this.menu = {parentName:null,parentId:0,type:1,orderNum:0};
			this.getMenu();
		},
		update: function (event) {
			var menuId = this.getSelectedRow();
			if(menuId == null){
				return ;
			}
			
			$.get("../sys/menu/info/"+menuId, function(r){
				vm.showList = false;
                vm.title = "修改";
                vm.menu = r.menu;
                
                vm.getMenu();
            });
		},
		del: function (event) {
			var menuIds = this.getSelectedRows();
			if(menuIds == null){
				return ;
			}
			
			layer.confirm('确定要删除选中的记录？', function(){
				var layer_index;
				$.ajax({
					type: "POST",
				    url: "../sys/menu/delete",
				    data: JSON.stringify(menuIds),
				    beforeSend:function(){
				    	layer_index = layer.load(0, {shade: false});
				    },
				    success: function(r){
				    	layer.close(layer_index);
				    	if(r.code === 0){
							layer.alert('操作成功', function(index){
								vm.reload();
							});
						}else{
							layer.alert(r.msg);
						}
					}
				});
			});
		},
		saveOrUpdate: function (event) {
			if(this.menu.name == undefined || this.menu.name ==''){
				layer.alert('菜单名称', {
					  icon: 5
					});
				return false;
			}
			var layer_index;
			var url = this.menu.menuId == null ? "../sys/menu/save" : "../sys/menu/update";
			$.ajax({
				type: "POST",
			    url: url,
			    data: JSON.stringify(vm.menu),
			    beforeSend:function(){
			    	layer_index = layer.load(0, {shade: false});
			    },
			    success: function(r){
			    	layer.close(layer_index);
			    	if(r.code === 0){
						alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						alert(r.msg);
					}
				},
				error: function(){
					layer.close(layer_index);
					layer.alert('操作失败');
				}
			});
		},
		menuTree: function(){
			layer.open({
				type: 1,
				offset: '50px',
				skin: 'layui-layer-molv',
				title: "选择菜单",
				area: ['300px', '450px'],
				shade: 0,
				shadeClose: false,
				content: jQuery("#menuLayer"),
				btn: ['确定', '取消'],
				btn1: function (index) {
					var node = ztree.getSelectedNodes();
					//选择上级菜单
					vm.menu.parentId = node[0].menuId;
					vm.menu.parentName = node[0].name;
					
					layer.close(index);
	            }
			});
		},
		reload: function (event) {
			this.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		},
		updatePassword: function(){
			layer.open({
				type: 1,
				skin: 'layui-layer-molv',
				title: "修改密码",
				area: ['550px', '270px'],
				shadeClose: false,
				content: $("#passwordLayer"),
				btn: ['修改','取消'],
				btn1: function (index) {
					if(vm.password ==''){
						layer.close(index);
						layer.alert('原密码不能为空', {
							  icon: 5
							});
						return false;
					}
					if(vm.newPassword == ''){
						layer.close(index);
						layer.alert('新密码不能为空', {
							  icon: 5
							});
						return false;
					}
					var data = "password="+vm.password+"&newPassword="+vm.newPassword;
					$.ajax({
						type: "POST",
					    url: "sys/user/password",
					    data: data,
					    dataType: "json",
					    success: function(result){
							if(result.code == 0){
								layer.close(index);
								layer.alert('修改成功', function(index){
									location.reload();
								});
							}else{
								layer.alert(result.msg);
							}
						}
					});
	            }
			});
		},
		doLogout:function(){
			$.getJSON("/sys/logout?_"+$.now(), function(r){
				store.remove('menuList');
				store.remove('permissions');
				store.remove('user');
				this.user = {};
				this.menuList = {};
				this.permissions = {};
				location.href='login.html';
			});
		},
		hasPermission:function(permission){
			if(this.permissions.indexOf(permission) > -1){
				return true;
			}else{
				return false;
			}
		},
		getSelectedRow:function(){
			var grid = $("#jqGrid");
		    var rowKey = grid.getGridParam("selrow");
		    if(!rowKey){
		    	layer.alert("请选择一条记录");
		    	return ;
		    }
		    
		    var selectedIDs = grid.getGridParam("selarrrow");
		    if(selectedIDs.length > 1){
		    	layer.alert("只能选择一条记录");
		    	return ;
		    }
		    
		    return selectedIDs[0];
		},
		getSelectedRows:function(){
			var grid = $("#jqGrid");
		    var rowKey = grid.getGridParam("selrow");
		    if(!rowKey){
		    	layer.alert("请选择一条记录");
		    	return ;
		    }
		    
		    return grid.getGridParam("selarrrow");
		}
	}
});