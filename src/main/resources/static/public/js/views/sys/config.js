$(function () {
	$.jgrid.defaults.width = 1000;
	$.jgrid.defaults.responsive = true;
	$.jgrid.defaults.styleUI = 'Bootstrap';
	
	var $jqGrid =  $("#jqGrid");
	$jqGrid.jqGrid({
        url: '../sys/config/list',
        datatype: "json",
        colModel: [			
			{ label: 'ID', name: 'id', width: 30, key: true },
			{ label: '参数名', name: 'key', width: 60 },
			{ label: '参数值', name: 'value', width: 100 },
			{ label: '备注', name: 'remark', width: 80 }
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$jqGrid.closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
			key: null
		},
		showList: true,
		title: null,
		config: {},
		menuList:store.get('menuList'),
		permissions:store.get('permissions'),
		username:store.get('user').username,
		password:'',
		newPassword:''
	},
	methods: {
		query: function () {
			this.reload();
		},
		add: function(){
			this.showList = false;
			this.title = "新增";
			this.config = {};
		},
		update: function () {
			var id = this.getSelectedRow();
			if(id == null){
				return ;
			}
			
			$.get("../sys/config/info/"+id, function(r){
                vm.showList = false;
                vm.title = "修改";
                vm.config = r.config;
            });
		},
		del: function (event) {
			var ids = this.getSelectedRows();
			if(ids == null){
				return ;
			}
			
			layer.confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: "../sys/config/delete",
				    data: JSON.stringify(ids),
				    success: function(r){
						if(r.code == 0){
							layer.alert('操作成功', function(index){
								vm.reload();
							});
						}else{
							layer.alert(r.msg);
						}
					}
				});
			});
		},
		saveOrUpdate: function (event) {
			var url = this.config.id == null ? "../sys/config/save" : "../sys/config/update";
			$.ajax({
				type: "POST",
			    url: url,
			    data: JSON.stringify(vm.config),
			    success: function(r){
			    	if(r.code === 0){
						layer.alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						layer.alert(r.msg);
					}
				}
			});
		},
		reload: function (event) {
			this.showList = true;
			var page = $("#jqGrid").jqGrid('getGridParam','page');
			$("#jqGrid").jqGrid('setGridParam',{ 
                postData:{'key': vm.q.key},
                page:page
            }).trigger("reloadGrid");
		},
		updatePassword: function(){
			layer.open({
				type: 1,
				skin: 'layui-layer-molv',
				title: "修改密码",
				area: ['550px', '270px'],
				shadeClose: false,
				content: $("#passwordLayer"),
				btn: ['修改','取消'],
				btn1: function (index) {
					if(vm.password ==''){
						layer.close(index);
						layer.alert('原密码不能为空', {
							  icon: 5
							});
						return false;
					}
					if(vm.newPassword == ''){
						layer.close(index);
						layer.alert('新密码不能为空', {
							  icon: 5
							});
						return false;
					}
					var data = "password="+vm.password+"&newPassword="+vm.newPassword;
					$.ajax({
						type: "POST",
					    url: "sys/user/password",
					    data: data,
					    dataType: "json",
					    success: function(result){
							if(result.code == 0){
								layer.close(index);
								layer.alert('修改成功', function(index){
									location.reload();
								});
							}else{
								layer.alert(result.msg);
							}
						}
					});
	            }
			});
		},
		doLogout:function(){
			$.getJSON("/sys/logout?_"+$.now(), function(r){
				store.remove('menuList');
				store.remove('permissions');
				store.remove('user');
				this.user = {};
				this.menuList = {};
				this.permissions = {};
				location.href='login.html';
			});
		},
		hasPermission:function(permission){
			if(this.permissions.indexOf(permission) > -1){
				return true;
			}else{
				return false;
			}
		},
		getSelectedRow:function(){
			var grid = $("#jqGrid");
		    var rowKey = grid.getGridParam("selrow");
		    if(!rowKey){
		    	layer.alert("请选择一条记录");
		    	return ;
		    }
		    
		    var selectedIDs = grid.getGridParam("selarrrow");
		    if(selectedIDs.length > 1){
		    	layer.alert("只能选择一条记录");
		    	return ;
		    }
		    
		    return selectedIDs[0];
		},
		getSelectedRows:function(){
			var grid = $("#jqGrid");
		    var rowKey = grid.getGridParam("selrow");
		    if(!rowKey){
		    	layer.alert("请选择一条记录");
		    	return ;
		    }
		    
		    return grid.getGridParam("selarrrow");
		}
	}
});