$(function () {
	$.jgrid.defaults.width = 1000;
	$.jgrid.defaults.responsive = true;
	$.jgrid.defaults.styleUI = 'Bootstrap';
	
	var $jqGrid =  $("#jqGrid");
	$jqGrid.jqGrid({
        url: '../sys/oss/list',
        datatype: "json",
        colModel: [			
			{ label: 'id', name: 'id', width: 20, key: true },
            { label: 'URL地址', name: 'url', width: 160 },
			{ label: '创建时间', name: 'createDate', width: 40 }
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$jqGrid.closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });

    new AjaxUpload('#upload', {
        action: '../sys/oss/upload',
        name: 'file',
        autoSubmit:true,
        responseType:"json",
        onSubmit:function(file, extension){
            if(vm.config.type == null){
                layer.alert("云存储配置未配置");
                return false;
            }
            if (!(extension && /^(jpg|jpeg|png|gif)$/.test(extension.toLowerCase()))){
                alert('只支持jpg、png、gif格式的图片！');
                return false;
            }
        },
        onComplete : function(file, r){
            if(r.code == 0){
                layer.alert(r.url);
                vm.reload();
            }else{
                layer.alert(r.msg);
            }
        }
    });

});

var vm = new Vue({
	el:'#rrapp',
	data:{
		showList: true,
		title: null,
        config: {},
        menuList:store.get('menuList'),
		permissions:store.get('permissions'),
		username:store.get('user').username,
		password:'',
		newPassword:''
	},
    created: function(){
        this.getConfig();
    },
	methods: {
		query: function () {
			this.reload();
		},
		getConfig: function () {
            $.getJSON("../sys/oss/config", function(r){
				vm.config = r.config;
            });
        },
		addConfig: function(){
			this.showList = false;
			this.title = "云存储配置";
		},
		saveOrUpdate: function () {
			var url = "../sys/oss/saveConfig";
			$.ajax({
				type: "POST",
			    url: url,
			    data: JSON.stringify(vm.config),
			    success: function(r){
			    	if(r.code === 0){
						layer.alert('操作成功', function(){
							vm.reload();
						});
					}else{
						layer.alert(r.msg);
					}
				}
			});
		},
        del: function () {
            var ossIds = this.getSelectedRows();
            if(ossIds == null){
                return ;
            }

            confirm('确定要删除选中的记录？', function(){
                $.ajax({
                    type: "POST",
                    url: "../sys/oss/delete",
                    data: JSON.stringify(ossIds),
                    success: function(r){
                        if(r.code === 0){
                            layer.alert('操作成功', function(){
                                vm.reload();
                            });
                        }else{
                            layer.alert(r.msg);
                        }
                    }
                });
            });
        },
		reload: function () {
			this.showList = true;
			var $dom_jqGrid = $("#jqGrid");
			var page = $dom_jqGrid.jqGrid('getGridParam','page');
			$dom_jqGrid.jqGrid('setGridParam',{ 
                page:page
            }).trigger("reloadGrid");
		},
		updatePassword: function(){
			layer.open({
				type: 1,
				skin: 'layui-layer-molv',
				title: "修改密码",
				area: ['550px', '270px'],
				shadeClose: false,
				content: $("#passwordLayer"),
				btn: ['修改','取消'],
				btn1: function (index) {
					if(vm.password ==''){
						layer.close(index);
						layer.alert('原密码不能为空', {
							  icon: 5
							});
						return false;
					}
					if(vm.newPassword == ''){
						layer.close(index);
						layer.alert('新密码不能为空', {
							  icon: 5
							});
						return false;
					}
					var data = "password="+vm.password+"&newPassword="+vm.newPassword;
					$.ajax({
						type: "POST",
					    url: "sys/user/password",
					    data: data,
					    dataType: "json",
					    success: function(result){
							if(result.code == 0){
								layer.close(index);
								layer.alert('修改成功', function(index){
									location.reload();
								});
							}else{
								layer.alert(result.msg);
							}
						}
					});
	            }
			});
		},
		doLogout:function(){
			$.getJSON("/sys/logout?_"+$.now(), function(r){
				store.remove('menuList');
				store.remove('permissions');
				store.remove('user');
				this.user = {};
				this.menuList = {};
				this.permissions = {};
				location.href='login.html';
			});
		},
		hasPermission:function(permission){
			if(this.permissions.indexOf(permission) > -1){
				return true;
			}else{
				return false;
			}
		},
		getSelectedRow:function(){
			var grid = $("#jqGrid");
		    var rowKey = grid.getGridParam("selrow");
		    if(!rowKey){
		    	layer.alert("请选择一条记录");
		    	return ;
		    }
		    
		    var selectedIDs = grid.getGridParam("selarrrow");
		    if(selectedIDs.length > 1){
		    	layer.alert("只能选择一条记录");
		    	return ;
		    }
		    
		    return selectedIDs[0];
		},
		getSelectedRows:function(){
			var grid = $("#jqGrid");
		    var rowKey = grid.getGridParam("selrow");
		    if(!rowKey){
		    	layer.alert("请选择一条记录");
		    	return ;
		    }
		    
		    return grid.getGridParam("selarrrow");
		}
	}
});