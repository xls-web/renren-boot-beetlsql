
var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
			username: null
		},
		showList: true,
		title:null,
		roleList:{},
		user:{
			status:1,
			roleIdList:[]
		},
		menuList:store.get('menuList'),
		permissions:store.get('permissions'),
		username:store.get('user').username,
		password:'',
		newPassword:''
	},
	methods: {
		query: function () {
			var layer_index = layer.load(0, {shade: false});
			this.reload();
			layer.close(layer_index);
		},
		add: function(){
			this.showList = false;
			this.title = "新增";
			this.roleList = {};
			this.user = {status:1,roleIdList:[]};
			
			//获取角色信息
			this.getRoleList();
		},
		update: function () {
			var userId = this.getSelectedRow();
			if(userId == null){
				return ;
			}
			
			this.showList = false;
			this.title = "修改";
			
			this.getUser(userId);
			//获取角色信息
			this.getRoleList();
		},
		del: function () {
			var userIds = this.getSelectedRows();
			if(userIds == null){
				return ;
			}
			
			layer.confirm('确定要删除选中的记录？', function(){
				var layer_index;
				$.ajax({
					type: "POST",
				    url: "../sys/user/delete",
				    data: JSON.stringify(userIds),
				    beforeSend:function(){
				    	layer_index = layer.load(0, {shade: false});
				    },
				    success: function(r){
				    	layer.close(layer_index);
						if(r.code == 0){
							layer.alert('操作成功', function(index){
                                vm.reload();
							});
						}else{
							layer.alert(r.msg);
						}
					}
				});
			});
		},
		saveOrUpdate: function (event) {
			if(this.user.username == undefined || this.user.username==''){
				layer.alert('账号不能为空', {
					  icon: 5
					});
				return false;
			}
			if(this.user.password == undefined || this.user.password==''){
				layer.alert('密码不能为空', {
					  icon: 5
					});
				return false;
			}
			var url = this.user.userId == null ? "../sys/user/save" : "../sys/user/update";
			var layer_index;
			$.ajax({
				type: "POST",
			    url: url,
			    data: JSON.stringify(vm.user),
			    beforeSend:function(){
			    	layer_index = layer.load(0, {shade: false});
			    },
			    success: function(r){
			    	layer.close(layer_index);
			    	if(r.code === 0){
			    		layer.alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						layer.alert(r.msg);
					}
				},
				error: function(){
					layer.close(layer_index);
					layer.alert('操作失败');
				}
			});
		},
		getUser: function(userId){
			$.get("../sys/user/info/"+userId, function(r){
				vm.user = r.user;
			});
		},
		getRoleList: function(){
			$.get("../sys/role/select", function(r){
				vm.roleList = r.list;
			});
		},
		reload: function (event) {
			vm.showList = true;
			var dom_jqGrid = $("#jqGrid");
			var page = dom_jqGrid.jqGrid('getGridParam','page');
			dom_jqGrid.jqGrid('setGridParam',{ 
                postData:{'username': vm.q.username},
                page:page
            }).trigger("reloadGrid");
		},
		updatePassword: function(){
			layer.open({
				type: 1,
				skin: 'layui-layer-molv',
				title: "修改密码",
				area: ['550px', '270px'],
				shadeClose: false,
				content: $("#passwordLayer"),
				btn: ['修改','取消'],
				btn1: function (index) {
					if(vm.password ==''){
						layer.close(index);
						layer.alert('原密码不能为空', {
							  icon: 5
							});
						return false;
					}
					if(vm.newPassword == ''){
						layer.close(index);
						layer.alert('新密码不能为空', {
							  icon: 5
							});
						return false;
					}
					var data = "password="+vm.password+"&newPassword="+vm.newPassword;
					$.ajax({
						type: "POST",
					    url: "sys/user/password",
					    data: data,
					    dataType: "json",
					    success: function(result){
							if(result.code == 0){
								layer.close(index);
								layer.alert('修改成功', function(index){
									location.reload();
								});
							}else{
								layer.alert(result.msg);
							}
						}
					});
	            }
			});
		},
		doLogout:function(){
			$.getJSON("/sys/logout?_"+$.now(), function(r){
				store.remove('menuList');
				store.remove('permissions');
				store.remove('user');
				this.user = {};
				this.menuList = {};
				this.permissions = {};
				location.href='login.html';
			});
		},
		hasPermission:function(permission){
			if(this.permissions.indexOf(permission) > -1){
				return true;
			}else{
				return false;
			}
		},
		getSelectedRow:function(){
			var grid = $("#jqGrid");
		    var rowKey = grid.getGridParam("selrow");
		    if(!rowKey){
		    	layer.alert("请选择一条记录");
		    	return ;
		    }
		    
		    var selectedIDs = grid.getGridParam("selarrrow");
		    if(selectedIDs.length > 1){
		    	layer.alert("只能选择一条记录");
		    	return ;
		    }
		    
		    return selectedIDs[0];
		},
		getSelectedRows:function(){
			var grid = $("#jqGrid");
		    var rowKey = grid.getGridParam("selrow");
		    if(!rowKey){
		    	layer.alert("请选择一条记录");
		    	return ;
		    }
		    
		    return grid.getGridParam("selarrrow");
		}
	}
});

$(function () {
	$.jgrid.defaults.width = 1000;
	$.jgrid.defaults.responsive = true;
	$.jgrid.defaults.styleUI = 'Bootstrap';
	
	var $jqGrid = $("table#jqGrid");
	$jqGrid.jqGrid({
        url: '../sys/user/list',
        datatype: "json",
        colModel: [			
			{ label: '用户ID', name: 'userId', index: "user_id", width: 45, key: true },
			{ label: '用户名', name: 'username', width: 75 },
			{ label: '邮箱', name: 'email', width: 90 },
			{ label: '手机号', name: 'mobile', width: 100 },
			{ label: '状态', name: 'status', width: 80, formatter: function(value, options, row){
				return value === 0 ? 
					'<span class="label label-danger">禁用</span>' : 
					'<span class="label label-success">正常</span>';
			}},
			{ label: '创建时间', name: 'createTime', index: "create_time", width: 80}
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$jqGrid.closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});