$(function () {
	$.jgrid.defaults.width = 1000;
	$.jgrid.defaults.responsive = true;
	$.jgrid.defaults.styleUI = 'Bootstrap';
	
	var $jqGrid =  $("#jqGrid");
	$jqGrid.jqGrid({
        url: '../sys/schedule/list',
        datatype: "json",
        colModel: [			
			{ label: '任务ID', name: 'jobId', width: 60, key: true },
			{ label: 'bean名称', name: 'beanName', width: 100 },
			{ label: '方法名称', name: 'methodName', width: 100 },
			{ label: '参数', name: 'params', width: 100 },
			{ label: 'cron表达式 ', name: 'cronExpression', width: 100 },
			{ label: '备注 ', name: 'remark', width: 100 },
			{ label: '状态', name: 'status', width: 60, formatter: function(value, options, row){
				return value === 0 ? 
					'<span class="label label-success">正常</span>' : 
					'<span class="label label-danger">暂停</span>';
			}}
        ],
		viewrecords: true,
        height: 385,
        rowNum: 10,
		rowList : [10,30,50],
        rownumbers: true, 
        rownumWidth: 25, 
        autowidth:true,
        multiselect: true,
        pager: "#jqGridPager",
        jsonReader : {
            root: "page.list",
            page: "page.currPage",
            total: "page.totalPage",
            records: "page.totalCount"
        },
        prmNames : {
            page:"page", 
            rows:"limit", 
            order: "order"
        },
        gridComplete:function(){
        	//隐藏grid底部滚动条
        	$jqGrid.closest(".ui-jqgrid-bdiv").css({ "overflow-x" : "hidden" }); 
        }
    });
});

var vm = new Vue({
	el:'#rrapp',
	data:{
		q:{
			jobId: null
		},
		showList: true,
		title: null,
		schedule: {},
		menuList:store.get('menuList'),
		permissions:store.get('permissions'),
		username:store.get('user').username,
		password:'',
		newPassword:''
	},
	methods: {
		query: function () {
			this.reload();
		},
		add: function(){
			this.showList = false;
			this.title = "新增";
			this.schedule = {};
		},
		update: function () {
			var jobId = this.getSelectedRow();
			if(jobId == null){
				return ;
			}
			
			$.get("../sys/schedule/info/"+jobId, function(r){
				vm.showList = false;
                vm.title = "修改";
				vm.schedule = r.schedule;
			});
		},
		saveOrUpdate: function (event) {
			var url = this.schedule.jobId == null ? "../sys/schedule/save" : "../sys/schedule/update";
			$.ajax({
				type: "POST",
			    url: url,
			    data: JSON.stringify(vm.schedule),
			    success: function(r){
			    	if(r.code === 0){
						layer.alert('操作成功', function(index){
							vm.reload();
						});
					}else{
						layer.alert(r.msg);
					}
				}
			});
		},
		del: function (event) {
			var jobIds = this.getSelectedRows();
			if(jobIds == null){
				return ;
			}
			
			layer.confirm('确定要删除选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: "../sys/schedule/delete",
				    data: JSON.stringify(jobIds),
				    success: function(r){
						if(r.code == 0){
							layer.alert('操作成功', function(index){
								vm.reload();
							});
						}else{
							layer.alert(r.msg);
						}
					}
				});
			});
		},
		pause: function (event) {
			var jobIds = this.getSelectedRows();
			if(jobIds == null){
				return ;
			}
			
			layer.confirm('确定要暂停选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: "../sys/schedule/pause",
				    data: JSON.stringify(jobIds),
				    success: function(r){
						if(r.code == 0){
							layer.alert('操作成功', function(index){
								vm.reload();
							});
						}else{
							layer.alert(r.msg);
						}
					}
				});
			});
		},
		resume: function (event) {
			var jobIds = this.getSelectedRows();
			if(jobIds == null){
				return ;
			}
			
			layer.confirm('确定要恢复选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: "../sys/schedule/resume",
				    data: JSON.stringify(jobIds),
				    success: function(r){
						if(r.code == 0){
							layer.alert('操作成功', function(index){
								vm.reload();
							});
						}else{
							layer.alert(r.msg);
						}
					}
				});
			});
		},
		runOnce: function (event) {
			var jobIds = this.getSelectedRows();
			if(jobIds == null){
				return ;
			}
			
			layer.confirm('确定要立即执行选中的记录？', function(){
				$.ajax({
					type: "POST",
				    url: "../sys/schedule/run",
				    data: JSON.stringify(jobIds),
				    success: function(r){
						if(r.code == 0){
							layer.alert('操作成功', function(index){
								vm.reload();
							});
						}else{
							layer.alert(r.msg);
						}
					}
				});
			});
		},
		reload: function (event) {
			this.showList = true;
			var $dom_jqGrid = $("#jqGrid");
			var page = $dom_jqGrid.jqGrid('getGridParam','page');
			$dom_jqGrid.jqGrid('setGridParam',{ 
                postData:{'beanName': vm.q.jobId},
                page:page 
            }).trigger("reloadGrid");
		},
		updatePassword: function(){
			layer.open({
				type: 1,
				skin: 'layui-layer-molv',
				title: "修改密码",
				area: ['550px', '270px'],
				shadeClose: false,
				content: $("#passwordLayer"),
				btn: ['修改','取消'],
				btn1: function (index) {
					if(vm.password ==''){
						layer.close(index);
						layer.alert('原密码不能为空', {
							  icon: 5
							});
						return false;
					}
					if(vm.newPassword == ''){
						layer.close(index);
						layer.alert('新密码不能为空', {
							  icon: 5
							});
						return false;
					}
					var data = "password="+vm.password+"&newPassword="+vm.newPassword;
					$.ajax({
						type: "POST",
					    url: "sys/user/password",
					    data: data,
					    dataType: "json",
					    success: function(result){
							if(result.code == 0){
								layer.close(index);
								layer.alert('修改成功', function(index){
									location.reload();
								});
							}else{
								layer.alert(result.msg);
							}
						}
					});
	            }
			});
		},
		doLogout:function(){
			$.getJSON("/sys/logout?_"+$.now(), function(r){
				store.remove('menuList');
				store.remove('permissions');
				store.remove('user');
				this.user = {};
				this.menuList = {};
				this.permissions = {};
				location.href='login.html';
			});
		},
		hasPermission:function(permission){
			if(this.permissions.indexOf(permission) > -1){
				return true;
			}else{
				return false;
			}
		},
		getSelectedRow:function(){
			var grid = $("#jqGrid");
		    var rowKey = grid.getGridParam("selrow");
		    if(!rowKey){
		    	layer.alert("请选择一条记录");
		    	return ;
		    }
		    
		    var selectedIDs = grid.getGridParam("selarrrow");
		    if(selectedIDs.length > 1){
		    	layer.alert("只能选择一条记录");
		    	return ;
		    }
		    
		    return selectedIDs[0];
		},
		getSelectedRows:function(){
			var grid = $("#jqGrid");
		    var rowKey = grid.getGridParam("selrow");
		    if(!rowKey){
		    	layer.alert("请选择一条记录");
		    	return ;
		    }
		    
		    return grid.getGridParam("selarrrow");
		}
	}
});