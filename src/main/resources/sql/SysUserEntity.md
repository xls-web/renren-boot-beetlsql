
queryList
===

* 分页查询

	select  
	@pageTag(){
	*
	@}  
	FROM  sys_user
	where true
    @if(!isEmpty(username)){
       and username like #'%'+username+'%'#
    @}
    @if(!isEmpty(createUserId)){
       and `create_user_id` = #createUserId#
    @}
    

queryAllPerms
===

	select m.perms from sys_user_role ur 
			LEFT JOIN sys_role_menu rm on ur.role_id = rm.role_id 
			LEFT JOIN sys_menu m on rm.menu_id = m.menu_id 
		where ur.user_id = #userId#
		
queryAllMenuId
===
	select distinct rm.menu_id from sys_user_role ur 
			LEFT JOIN sys_role_menu rm on ur.role_id = rm.role_id 
		where ur.user_id = #userId#
	

queryByUserName
===	
	select * from sys_user where username = #username#


updatePassword
===
	update sys_user set `password` = #newPassword# 
			where user_id = #userId# and password = #password#

