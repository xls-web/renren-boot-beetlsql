
queryList
===

* 分页查询

	select  
	@pageTag(){
	m.*,(select p.name from sys_menu p where p.menu_id = m.parent_id) as parentName
	@}  
	FROM  sys_menu m
    @if(!isEmpty(sidx)){
       order by m.#sidx# #order#
    @}
    @else{
    	 order by m.order_num asc
    @}
    

queryListParentId
===

	select * from sys_menu where parent_id = #parentId# order by order_num asc
	
queryNotButtonList
===
   
    select * from sys_menu where type != 2 order by order_num asc

queryUserList
===

	select distinct m.*,(select p.name from sys_menu p where p.menu_id = m.parent_id) as parentName
			from sys_user_role ur 
			LEFT JOIN sys_role_menu rm on ur.role_id = rm.role_id 
			LEFT JOIN sys_menu m on rm.menu_id = m.menu_id 
		where ur.user_id = #userId# 
		order by m.order_num asc   

	


