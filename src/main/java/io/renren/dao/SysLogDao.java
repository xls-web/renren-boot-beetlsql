package io.renren.dao;

import io.renren.entity.SysLogEntity;

/**
 * 系统日志
 * 
 */
public interface SysLogDao extends BaseDao<SysLogEntity> {
	
}
