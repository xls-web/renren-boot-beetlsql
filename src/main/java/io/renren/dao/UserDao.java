package io.renren.dao;

import org.beetl.sql.core.annotatoin.SqlStatement;

import io.renren.entity.UserEntity;

/**
 * 用户
 * 
 */
public interface UserDao extends BaseDao<UserEntity> {

	@SqlStatement(params="mobile")
    UserEntity queryByMobile(String mobile);
}
