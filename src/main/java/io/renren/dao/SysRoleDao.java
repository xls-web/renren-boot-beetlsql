package io.renren.dao;

import io.renren.entity.SysRoleEntity;

import java.util.List;

import org.beetl.sql.core.annotatoin.SqlStatement;

/**
 * 角色管理
 * 
 */
public interface SysRoleDao extends BaseDao<SysRoleEntity> {
	
	/**
	 * 查询用户创建的角色ID列表
	 */
	@SqlStatement(params="createUserId")
	List<Long> queryRoleIdList(Long createUserId);
}
