package io.renren.dao;

import io.renren.entity.SysRoleMenuEntity;

import java.util.List;

import org.beetl.sql.core.annotatoin.SqlStatement;

/**
 * 角色与菜单对应关系
 * 
 */
public interface SysRoleMenuDao extends BaseDao<SysRoleMenuEntity> {
	
	/**
	 * 根据角色ID，获取菜单ID列表
	 */
	@SqlStatement(params="roleId")
	List<Long> queryMenuIdList(Long roleId);
	
	@SqlStatement(params="roleId")
	void delete(Long roleId);
}
