package io.renren.dao;

import io.renren.entity.SysOssEntity;

/**
 * 文件上传
 */
public interface SysOssDao extends BaseDao<SysOssEntity> {
	
}
