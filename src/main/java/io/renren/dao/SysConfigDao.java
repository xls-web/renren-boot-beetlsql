package io.renren.dao;

import org.beetl.sql.core.annotatoin.SqlStatement;

import io.renren.entity.SysConfigEntity;

/**
 * 系统配置信息
 * 
 */
public interface SysConfigDao extends BaseDao<SysConfigEntity> {
	
	/**
	 * 根据key，查询value
	 */
	@SqlStatement(params="key")
	String queryByKey(String paramKey);
	
	/**
	 * 根据key，更新value
	 */
	@SqlStatement(params="key,value")
	int updateValueByKey(String key, String value);
	
}
