package io.renren.dao;

import io.renren.entity.SysUserEntity;

import java.util.List;
import java.util.Map;

import org.beetl.sql.core.annotatoin.SqlStatement;

/**
 * 系统用户
 * 
 */
public interface SysUserDao extends BaseDao<SysUserEntity> {
	
	/**
	 * 查询用户的所有权限
	 * @param userId  用户ID
	 */
	@SqlStatement(params="userId")
	List<String> queryAllPerms(Long userId);
	
	/**
	 * 查询用户的所有菜单ID
	 */
	@SqlStatement(params="userId")
	List<Long> queryAllMenuId(Long userId);
	
	/**
	 * 根据用户名，查询系统用户
	 */
	@SqlStatement(params="username")
	SysUserEntity queryByUserName(String username);
	
	/**
	 * 修改密码
	 */
	int updatePassword(Map<String, Object> map);
}
