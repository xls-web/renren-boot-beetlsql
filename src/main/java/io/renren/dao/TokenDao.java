package io.renren.dao;

import org.beetl.sql.core.annotatoin.SqlStatement;

import io.renren.entity.TokenEntity;

/**
 * 用户Token
 * 
 */
public interface TokenDao extends BaseDao<TokenEntity> {
    
	@SqlStatement(params="userId")
    TokenEntity queryByUserId(Long userId);

	@SqlStatement(params="token")
    TokenEntity queryByToken(String token);
	
}
