package io.renren.dao;

import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.core.mapper.BaseMapper;

/**
 * 基础Dao(还需在XML文件里，有对应的SQL语句)
 * 
 */
public interface BaseDao<T> extends BaseMapper<T>{
	void queryList(PageQuery<T> query);
}
