package io.renren.dao;

import io.renren.entity.SysUserRoleEntity;

import java.util.List;

import org.beetl.sql.core.annotatoin.SqlStatement;

/**
 * 用户与角色对应关系
 * 
 */
public interface SysUserRoleDao extends BaseDao<SysUserRoleEntity> {
	
	/**
	 * 根据用户ID，获取角色ID列表
	 */
	@SqlStatement(params="userId")
	List<Long> queryRoleIdList(Long userId);
	
	@SqlStatement(params="userId")
	void delete(Long userId);
}
