package io.renren.dao;

import io.renren.entity.SysMenuEntity;

import java.util.List;

import org.beetl.sql.core.annotatoin.SqlStatement;

/**
 * 菜单管理
 * 
 */
public interface SysMenuDao extends BaseDao<SysMenuEntity> {
	
	/**
	 * 根据父菜单，查询子菜单
	 * @param parentId 父菜单ID
	 */
	@SqlStatement(params="parentId")
	List<SysMenuEntity> queryListParentId(Long parentId);
	
	/**
	 * 获取不包含按钮的菜单列表
	 */
	List<SysMenuEntity> queryNotButtonList();
	
	/**
	 * 查询用户的权限列表
	 */
	@SqlStatement(params="userId")
	List<SysMenuEntity> queryUserList(Long userId);
}
