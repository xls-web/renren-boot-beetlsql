package io.renren.service;

import java.util.List;
import java.util.Set;

import io.renren.entity.SysMenuEntity;


/**
 * 菜单管理
 * 
 */
public interface SysMenuService extends BaseService<SysMenuEntity>{
	
	List<SysMenuEntity> queryAllList();
	
	/**
	 * 根据父菜单，查询子菜单
	 * @param parentId 父菜单ID
	 * @param menuIdList  用户菜单ID
	 */
	List<SysMenuEntity> queryListParentId(Long parentId, List<Long> menuIdList);
	
	/**
	 * 获取不包含按钮的菜单列表
	 */
	List<SysMenuEntity> queryNotButtonList();
	
	/**
	 * 获取用户菜单列表
	 */
	List<SysMenuEntity> getUserMenuList(Long userId);

	/**
	 * 获取用户权限列表
	 */
	Set<String> getUserPermissions(long userId);
	
	/**
	 * 查询用户的权限列表
	 */
	List<SysMenuEntity> queryUserList(Long userId);
}
