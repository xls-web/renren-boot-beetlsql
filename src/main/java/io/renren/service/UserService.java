package io.renren.service;

import io.renren.entity.UserEntity;

/**
 * 用户
 * 
 */
public interface UserService extends BaseService<UserEntity>{
	void save(String mobile, String password);

	UserEntity queryByMobile(String mobile);

	/**
	 * 用户登录
	 * @param mobile    手机号
	 * @param password  密码
	 * @return          返回用户ID
	 */
	long login(String mobile, String password);
}
