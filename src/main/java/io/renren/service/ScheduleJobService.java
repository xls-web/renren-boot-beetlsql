package io.renren.service;

import io.renren.entity.ScheduleJobEntity;

/**
 * 定时任务
 * 
 */
public interface ScheduleJobService extends BaseService<ScheduleJobEntity>{
	
	/**
	 * 批量更新定时任务状态
	 */
	int updateBatch(Long[] jobIds, int status);
	
	/**
	 * 立即执行
	 */
	void run(Long[] jobIds);
	
	/**
	 * 暂停运行
	 */
	void pause(Long[] jobIds);
	
	/**
	 * 恢复运行
	 */
	void resume(Long[] jobIds);
}
