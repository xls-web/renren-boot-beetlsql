package io.renren.service;

import java.util.List;

import io.renren.entity.SysRoleEntity;


/**
 * 角色
 * 
 */
public interface SysRoleService extends BaseService<SysRoleEntity>{
	
	/**
	 * 查询用户创建的角色ID列表
	 */
	List<Long> queryRoleIdList(Long createUserId);
}
