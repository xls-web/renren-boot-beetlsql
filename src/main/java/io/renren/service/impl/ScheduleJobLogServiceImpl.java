package io.renren.service.impl;

import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.renren.dao.ScheduleJobLogDao;
import io.renren.entity.ScheduleJobLogEntity;
import io.renren.service.ScheduleJobLogService;

@Service("scheduleJobLogService")
public class ScheduleJobLogServiceImpl implements ScheduleJobLogService {
	@Autowired
	private ScheduleJobLogDao scheduleJobLogDao;

	@Override
	public ScheduleJobLogEntity queryObject(Object id) {
		return scheduleJobLogDao.unique(id);
	}

	@Override
	public void queryList(PageQuery<ScheduleJobLogEntity> query) {
		scheduleJobLogDao.queryList(query);
	}

	@Override
	@Transactional
	public void save(ScheduleJobLogEntity t) {
		scheduleJobLogDao.insert(t);
	}

	@Override
	@Transactional
	public void update(ScheduleJobLogEntity t) {
		scheduleJobLogDao.updateById(t);
	}

	@Override
	@Transactional
	public void delete(Object id) {
		scheduleJobLogDao.deleteById(id);
	}

	@Override
	@Transactional
	public void deleteBatch(Object[] ids) {
		for (Object id : ids) {
			scheduleJobLogDao.deleteById(id);
		}
	}

}
