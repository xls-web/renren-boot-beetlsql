package io.renren.service.impl;

import java.util.List;

import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.renren.dao.SysLogDao;
import io.renren.entity.SysLogEntity;
import io.renren.service.SysLogService;



@Service("sysLogService")
public class SysLogServiceImpl implements SysLogService {
	@Autowired
	private SysLogDao sysLogDao;

	@Override
	public SysLogEntity queryObject(Object id) {
		return sysLogDao.unique(id);
	}

	@Override
	public void queryList(PageQuery<SysLogEntity> query) {
		sysLogDao.queryList(query);
	}

	@Override
	@Transactional
	public void save(SysLogEntity t) {
		sysLogDao.insert(t);
	}

	@Override
	@Transactional
	public void update(SysLogEntity t) {
		sysLogDao.updateById(t);
	}

	@Override
	@Transactional
	public void delete(Object id) {
		sysLogDao.deleteById(id);
	}

	@Override
	@Transactional
	public void deleteBatch(Object[] ids) {
		for (Object id : ids) {
			sysLogDao.deleteById(id);
		}
	}

	@Override
	@Transactional
	public void saveBatch(List<SysLogEntity> logs) {
		// TODO Auto-generated method stub
		sysLogDao.insertBatch(logs);
	}
	
	
}
