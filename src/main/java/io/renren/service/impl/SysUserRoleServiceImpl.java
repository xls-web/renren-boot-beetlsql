package io.renren.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.renren.dao.SysUserRoleDao;
import io.renren.entity.SysUserRoleEntity;
import io.renren.service.SysUserRoleService;



/**
 * 用户与角色对应关系
 * 
 */
@Service("sysUserRoleService")
public class SysUserRoleServiceImpl implements SysUserRoleService {
	@Autowired
	private SysUserRoleDao sysUserRoleDao;

	@Override
	public void saveOrUpdate(Long userId, List<Long> roleIdList) {
		if(roleIdList.size() == 0){
			return ;
		}
		
		//先删除用户与角色关系
		sysUserRoleDao.delete(userId);
		
		//保存用户与角色关系
		for (Long roleId : roleIdList) {
			SysUserRoleEntity ur = new SysUserRoleEntity();
			ur.setUserId(userId);
			ur.setRoleId(roleId);
			sysUserRoleDao.insert(ur);
		}
		
	}

	@Override
	public List<Long> queryRoleIdList(Long userId) {
		return sysUserRoleDao.queryRoleIdList(userId);
	}

	@Override
	public void delete(Long userId) {
		sysUserRoleDao.delete(userId);
	}
}
