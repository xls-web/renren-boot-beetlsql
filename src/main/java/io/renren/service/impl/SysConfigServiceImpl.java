package io.renren.service.impl;

import org.apache.commons.lang.StringUtils;
import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import io.renren.dao.SysConfigDao;
import io.renren.entity.SysConfigEntity;
import io.renren.service.SysConfigService;
import io.renren.utils.RRException;

@Service("sysConfigService")
public class SysConfigServiceImpl implements SysConfigService {
	@Autowired
	private SysConfigDao sysConfigDao;
	
	@Override
	@Transactional
	public void updateValueByKey(String key, String value) {
		sysConfigDao.updateValueByKey(key, value);
	}

	@Override
	public String getValue(String key, String defaultValue) {
		String value = sysConfigDao.queryByKey(key);
		if(StringUtils.isBlank(value)){
			return defaultValue;
		}
		return value;
	}
	
	@Override
	public <T> T getConfigObject(String key, Class<T> clazz) {
		String value = getValue(key, null);
		if(StringUtils.isNotBlank(value)){
			return JSON.parseObject(value, clazz);
		}

		try {
			return clazz.newInstance();
		} catch (Exception e) {
			throw new RRException("获取参数失败");
		}
	}


	@Override
	public SysConfigEntity queryObject(Object id) {
		return sysConfigDao.unique(id);
	}


	@Override
	public void queryList(PageQuery<SysConfigEntity> query) {
		sysConfigDao.queryList(query);
	}


	@Override
	@Transactional
	public void save(SysConfigEntity t) {
		sysConfigDao.insert(t);
	}


	@Override
	@Transactional
	public void update(SysConfigEntity t) {
		sysConfigDao.updateById(t);
	}


	@Override
	@Transactional
	public void delete(Object id) {
		sysConfigDao.deleteById(id);
	}


	@Override
	@Transactional
	public void deleteBatch(Object[] ids) {
		for (Object id : ids) {
			sysConfigDao.deleteById(id);
		}
	}
}
