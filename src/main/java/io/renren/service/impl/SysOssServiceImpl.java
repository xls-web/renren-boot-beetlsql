package io.renren.service.impl;

import org.beetl.sql.core.engine.PageQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.renren.dao.SysOssDao;
import io.renren.entity.SysOssEntity;
import io.renren.service.SysOssService;



@Service("sysOssService")
public class SysOssServiceImpl implements SysOssService {
	@Autowired
	private SysOssDao sysOssDao;

	@Override
	public SysOssEntity queryObject(Object id) {
		return sysOssDao.unique(id);
	}

	@Override
	public void queryList(PageQuery<SysOssEntity> query) {
		sysOssDao.queryList(query);
	}

	@Override
	@Transactional
	public void save(SysOssEntity t) {
		sysOssDao.insert(t);
	}

	@Override
	@Transactional
	public void update(SysOssEntity t) {
		sysOssDao.updateById(t);
	}

	@Override
	@Transactional
	public void delete(Object id) {
		sysOssDao.deleteById(id);
	}

	@Override
	@Transactional
	public void deleteBatch(Object[] ids) {
		for (Object id : ids) {
			sysOssDao.deleteById(id);
		}
		
	}
}
