package io.renren.service;

import io.renren.entity.SysConfigEntity;

/**
 * 系统配置信息
 * 
 */
public interface SysConfigService extends BaseService<SysConfigEntity>{
	
	
	/**
	 * 根据key，更新value
	 */
	public void updateValueByKey(String key, String value);
	
	
	/**
	 * 根据key，获取配置的value值
	 * 
	 * @param key           key
	 * @param defaultValue  缺省值
	 */
	public String getValue(String key, String defaultValue);
	
	/**
	 * 根据key，获取value的Object对象
	 * @param key    key
	 * @param clazz  Object对象
	 */
	public <T> T getConfigObject(String key, Class<T> clazz);
	
}
