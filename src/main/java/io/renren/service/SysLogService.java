package io.renren.service;

import java.util.List;

import io.renren.entity.SysLogEntity;

/**
 * 系统日志
 * 
 */
public interface SysLogService extends BaseService<SysLogEntity>{
	void saveBatch(List<SysLogEntity> logs);
}
