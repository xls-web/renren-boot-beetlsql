package io.renren.service;

import org.beetl.sql.core.engine.PageQuery;

/**
 * 基础service,继承它就拥有单表的增删改查和分页功能
 * @param <T>
 */
public interface BaseService<T> {

	T queryObject(Object id);
	
	void queryList(PageQuery<T> query);
	
	void save(T t);
	
	void update(T t);
	
	void delete(Object id);
	
	void deleteBatch(Object[] ids);
	
}
