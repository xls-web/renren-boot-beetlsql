package io.renren.service;

import io.renren.entity.ScheduleJobLogEntity;

/**
 * 定时任务日志
 * 
 */
public interface ScheduleJobLogService extends BaseService<ScheduleJobLogEntity>{
	
}
