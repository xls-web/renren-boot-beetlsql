package io.renren.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

/**
 * 异步任务配置示例
 */
@Configuration
@ConditionalOnProperty(name = "spring.asynctask.enabled",matchIfMissing = false)
@EnableAsync
public class AsyncTaskConfig {

	private static Logger logger = LoggerFactory.getLogger(AsyncTaskConfig.class);
	
	/** 线程池数量 */
	@Value("${spring.asynctask.pool.size:10}")
	private int corePoolSize;
	
	/** 线程池最大数 */
	@Value("${spring.asynctask.pool.size:10}")
	private int maxPoolSize;

	/** 性能 */
	@Value("${spring.asynctask.queue.capacity:10}")
	private int queueCapacity;
	
	/**线程前缀*/
	@Value("${spring.asynctask.queue.threadNamePrefix:szChinalifeExecutor-}")
	private String threadNamePrefix;

	@Bean
	public TaskExecutor taskExecutor() {
	    ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
	    taskExecutor.setCorePoolSize(corePoolSize);
	    taskExecutor.setMaxPoolSize(this.maxPoolSize);
	    taskExecutor.setQueueCapacity(this.queueCapacity);
	    taskExecutor.setThreadNamePrefix(this.threadNamePrefix);
	    taskExecutor.afterPropertiesSet();
	    return taskExecutor;
	}
	
}
