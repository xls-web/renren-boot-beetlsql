package io.renren.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

//参考：http://blog.csdn.net/catoop/article/details/50668896
@Configuration
@EnableSwagger2
public class Swagger2Config {
	
	@Value("${spring.swagger2.basePackage}")
	private String basePackage;
	
	@Value("${spring.swagger2.api.title:renren-security-boot使用Swagger2构建RESTful API}")
	private String title;
	
	@Value("${spring.swagger2.api.description:人人编程：http://www.renren.io}")
	private String description;
	
	@Value("${spring.swagger2.api.termsOfServiceUrl:http://www.renren.io}")
	private String termsOfServiceUrl;
	
	@Value("${spring.swagger2.api.contact:人人开源}")
	private String contact;
	
	@Value("${spring.swagger2.api.version:2.0}")
	private String version;

	@Bean
    public Docket createRestApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.basePackage(basePackage))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(title)
                .description(description)
                .termsOfServiceUrl(termsOfServiceUrl)
                .contact(new Contact(contact, "", ""))
                .version(version)
                .build();
    }

}
