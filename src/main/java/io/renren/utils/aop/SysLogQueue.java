package io.renren.utils.aop;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

import io.renren.entity.SysLogEntity;

public class SysLogQueue {
//	private static Logger logger = LoggerFactory.getLogger(SysLogQueue.class);

	/**
	 * 阻塞队列
	 */
	private static BlockingQueue<SysLogEntity> queue = new LinkedBlockingQueue<SysLogEntity>();
	
    public static void add(SysLogEntity sysLog) {  
    	queue.add(sysLog);  
    }  
  
    public static SysLogEntity poll() throws InterruptedException {  
        return queue.poll(1, TimeUnit.SECONDS);  
    }
}
