package io.renren.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 定时任务 演示 参考
 */
@Component
public class SimpleScheduler {
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	
	//${scheduler.taksk.cronExpression:0 0/1 * * * ?}
	//表示从配置文件中找scheduler.taksk.cronExpression，如果没有找到，则默认0 0/1 * * * ?
	@Scheduled(cron="${scheduler.task.cronExpression:0 0/1 * * * ?}") //每分钟执行一次
	public void statusCheck() {    
//		logger.info("每分钟执行一次。开始……");
//		logger.info("每分钟执行一次。结束。");
	}  

//	@Scheduled(fixedRate=20000)
	@Scheduled(fixedRateString="${scheduler.task.fixedRate:20000}")
	public void testTasks() {    
//		logger.info("每20秒执行一次。开始……");
//		logger.info("每20秒执行一次。结束。");
	}  
}
