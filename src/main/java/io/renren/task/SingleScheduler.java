package io.renren.task;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.PersistJobDataAfterExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 不允许多个任务同时进行的quartz示例
 * 保证多个任务间不会同时执行.所以在多任务执行时最好加上@DisallowConcurrentExecution
 * 有状态的任务@PersistJobDataAfterExecution
 */
@Component
@DisallowConcurrentExecution
@PersistJobDataAfterExecution
public class SingleScheduler {
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Scheduled(cron="${scheduler.task.cronExpression:0 0/1 * * * ?}") //每分钟执行一次
	public void statusCheck() {    
//		logger.info("每分钟执行一次。开始……");
//		logger.info("每分钟执行一次。结束。");
	} 
}
