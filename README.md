**项目说明** 
- 十分感谢原项目renren-security-boot(https://git.oschina.net/babaio/renren-security-boot)作者的许可，原项目是基于[renren-security](http://git.oschina.net/babaio/renren-security)，用Spring Boot实现的J2EE快速开发平台
- 现这个版本项目是将mybatis部分换成mybatis,UI方面是将adminlte换成coreUI
- 使用renren-security-boot搭建项目，只需编写30%左右代码，其余的代码交给系统自动生成
- 一个月的工作量，一周就能完成，剩余的时间可以陪家人、朋友、撩妹、钓凯子等，从此踏入高富帅、白富美行业
- 也是接私活的利器，能快速完成项目并交付，轻松赚取外快，实现财务自由，走向人生巅峰（接私活赚了钱，可以给作者打赏点辛苦费，让作者更有动力持续优化、完善）

**该版本明显缺陷**
- 页面没有路由规则，不懂怎么直接在页面<script>标签引用vue-router，编写vue的js代码
- 还没支持layui
- 由于我把代码生成器给换成beetl了，还没来得及切换过来

 **技术选型：** 
- 核心框架：Spring Boot 1.5
- 安全框架：Apache Shiro 1.3
- 视图框架：Spring MVC 4.3
- 持久层框架：Beetlsql 2.8.x
- 定时器：Quartz 2.3
- 数据库连接池：Druid 1.0
- 日志管理：SLF4J 1.7、Log4j
- 页面交互：Vue2.x


 **软件需求** 
- JDK1.8+
- MySQL5.5+
- Tomcat7.0+
- Maven3.0+



 **本地部署**
- 通过git下载源码
- 创建数据库renren-security-boot，数据库编码为UTF-8
- 执行doc/db.sql文件，初始化数据
- 修改application.properties文件，更新MySQL账号和密码
- Eclipse、IDEA运行RenrenApplication.java，则可启动项目
- 项目访问路径：http://localhost
- API文档路径：http://localhost/swagger-ui.html
- 账号密码：admin/admin